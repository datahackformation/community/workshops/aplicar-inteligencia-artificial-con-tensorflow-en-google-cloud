

En este DataLive tenemos 2 personas entusiastas del mundo de Data y Analytics

Temas:

Aquí conocerás cómo Aplicar Inteligencia Artificial con TensorFlow en Google Cloud

Panel:

👨‍💻 Diego Benavides, Data Architect BCP, Perú  

👨‍💻 Jesús Méndez, Data Engineer Chapter Lead Intercorp, Perú 

Link del Datalive YouTube:  https://www.youtube.com/watch?v=JjT25J6Q2l8&t=56s

Link del Datalive Anchor:   https://anchor.fm/data-hack-formation/episodes/DataLive-Gratuito-Inteligencia-Artificial-con-TensorFlow-egrft9

#AWS #Azure #GoogleCloud #GCP #Cloud #AI #ML #Kubernetes #BigQuery #ArtificialIntelligence #MachineLearning #BigData #DataEngineering #DataArchitect #DataScience #DataHackFormation

Aprende, certifícate y trabaja en proyectos de Data y Analytics en Cloud, empieza en https://datahacks.ai/